import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Community from './community';
import Join from './join';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <>
    <Community />
    <Join />
    </>
    {/* <Community /> */}
  </React.StrictMode>
);

