/* eslint-disable no-mixed-operators */
import React, { useState} from 'react';
import sendRequest from './sendRequest';

function Join() {
    const [btn, setBtn] = useState('SUBSCRIBE');

    const subscribe = () => {
        const input = document.getElementById('added-section__email-input');
        const email = input.value;
        const validEmailEndings = ['gmail.com', 'outlook.com', 'yandex.ru'];
        const ending = email.substring(email.indexOf('@') + 1);
        const start = email.substring(0, email.indexOf('@'));
        const form = document.getElementById('added-section__email-box');
        const button = document.getElementById('added-section__email-btn');

        if(email === 'forbidden@gmail.com') {
            alert("error : Email is already in use")
        } else if(ending === validEmailEndings[0] && start.length > 0 || 
            validEmailEndings[1] && start.length > 0 ||
            validEmailEndings[2] && start.length > 0) {
             localStorage.setItem('email', email);
             if (btn === "SUBSCRIBE") {
                input.style.display = 'none';
                button.style.height = '42px';
                setBtn('UNSUBSCRIBE')
                form.style.justifyContent = 'center';
                sendRequest(email, 'http://localhost:3000/subscribe', button);
            } else {
                input.style.display = 'inline-block';
                button.style.height = '42px';
                setBtn('SUBSCRIBE');
                form.style.justifyContent = 'space-between';
                localStorage.removeItem('email');
                input.value = '';
                sendRequest(email, 'http://localhost:3000/unsubscribe', button);
            }
        
        } else {
            alert('false')
        }
    }

    return (
        <section className='added-section'>
            <div className='added-section__content'>
                <h2 className='added-section__h2'>Join Our Program</h2>
                <p className='added-section__p'>Sed do eiusmod tempor incididunt<br/>ut labore et dolore magna aliqua.</p>
                <form className='added-section__email-box' id="added-section__email-box">
                    <input className='added-section__email-input' id='added-section__email-input' type="email" placeholder='Email'></input>
                    <button className='added-section__email-btn' id='added-section__email-btn' onClick={subscribe} type='button'>{btn}</button>
                </form>
            </div>
        </section>
    );
} 

export default Join;